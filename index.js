/**
 * @fileOverview
 * @name index.js<darkseas-utility>
 * @author Gavin Coombes
 * @license MIT
 *
 * Wrapper around utility library like lodash plus other utils I have picked along the way
 *
 */



var lodash = require('lodash');
// var lodashfp = require('lodash-fp');

var Utility = (function(utility) {

    utility.querify = function querify(obj) {
        // http://stackoverflow.com/questions/1714786/querystring-encoding-of-a-javascript-object
        var str = ['?'];
        for(var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    };

    utility.resolve = function resolve(obj) {
        return Promise.resolve(obj);
    };

  utility.log = Function.prototype.bind.call(console.log, console);
    // let module = {
    //     curry: R.curry,
    //     merge: R.merge,
    //     querify: querify
    // };

  utility.guid32 = function guid32() {
    // http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  };

  utility.isoDate = function utcdate() {
    return new Date().toISOString();
  };

  utility._s6 = function s6() {
      return Math.floor((1 + Math.random()) * 0x1000000)
        .toString(16)
        .substring(1);
  };

  if (typeof utility.isNil === 'undefined') {
    utility.isNil = function(v) {
      return utility.isUndefined(v) || utility.isNull(v);
    };
  }

  utility.guid = function guid(opts={}) {
    let defaults = {sep: '-', prefix: null, suffix: null};
    let o = utility.merge(defaults, opts);
    var elems = [utility.isoDate(), utility._s6()];
    if (!utility.isNil(o.prefix)) {elems = utility.prepend(o.prefix, elems);}
    if (!utility.isNil(o.suffix)) {elems = utility.append(o.suffix, elems);}
    return utility.join(o.sep, elems);
  };


  utility.thunk = function(f, ...args) {
    return () => f(...args);
  };

  // utility.partial = function(f, ...args) {
  //   return f.bind(f, ...args);
  // };

  utility.evergreen = function evergreen(obj = {}){
    /**
    evergreen: Return a function to ensure local cached value and send message on change.
    Args:
        obj: An object with three properties e.g.
        obj = {
        get     : Function to get a new value,
                    Defaults to identity of first input arg.
        compare : Predicate to return true if new value is different to previous value.
                    Defaults to strict comparison '!=='.
        swap    : Function to assign to previous value.
                    Defaults to identity of the new value.
        send    : Function to notify the store or view model of the new value.
                    Defaults to eat and log the new value.
        }
    */
    let _prev = undefined;  // Could probably be a symbol
    let get = obj.get || utility.identity;
    let compare = obj.compare || utility.compose(utility.not, utility.equals);
    let swap = obj.swap || function(_prev, _new) {_prev = _new; return _prev;};
    let send = obj.send || utility.log;

    let f = function(val) {
    // utility.log('evergreen: got val ', val);
    let _new = get(val);     // Get the new value
      // utility.log('evergreen: _new is ', _new);
    if (compare(_prev, _new)) {  // Is it different?
      // utility.log('evergreen: found difference prev ', _prev, ' new ', _new);
      _prev = swap(_prev, _new);        // If so, swap in the new value by value
      // utility.log('evergreen: after swap prev is ', _prev, ', new is ', _new);
      return send(_prev);         // and notify someone.
    } else {
        return _prev;
    }
    };
    return f;
  };

  utility.safe = function(...args) {
    let result = null;
    try {result = utility.call(...args);}
    // catch (e) {utility.log(e); }
    finally {return result;}
  };

  utility.$head = function(xs) {
    return utility.safe(utility.head, xs);
  };

  utility.$tail = function(xs) {
    return utility.safe(utility.tail, xs);
  };

  utility.prop = function prop(prev) {
    // Return a function a getter-setter [see mithril.js]
    var _prop = function(val) {
      if (arguments.length) { prev = val; }
      return prev;
    };
    _prop.toJSON = function() { return prev; };
    return _prop;
  };


    return utility;
})(lodash);

module.exports = Utility;
